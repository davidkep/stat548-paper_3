#' Variance-covariance matrix from the MGL model
#'
#'
#'
#' @param object The object returned from a call to \code{\link{MGL}}.
#' @param cov.output Should the covariance be returned as well.
#'
#' @return \item{covi}{The precision matrix}
#' @return \item{cov}{The covariance matrix (if \code{cov.output = TRUE})}
#' @export
vcov.mgl <- function(object, cov.output = TRUE, ...) {
    pData <- length(object$Z)
    pLatent <- ncol(object$L)
    moduleSizes <- tabulate(object$Z, nbins = pLatent)

    A <- diag(rep.int(1 / object$var.modules, times = moduleSizes), pData, pData)
    B <- object$theta + diag(moduleSizes / object$var.modules, pLatent, pLatent)

    Ct <- mapply(function(var, index) {
        r <- numeric(pData)
        r[object$Z == index] <- -1 / var
        return(r)
    },
    object$var.modules,
    seq_len(pLatent))

    SXinv <- A - Ct %*% solve(B, t(Ct))

    return(list(
        cov = if (cov.output == TRUE) { solve(SXinv) } else { NULL },
        covi = SXinv
    ))
}
