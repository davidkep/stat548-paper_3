/*
 * BLAS.h
 * MGL2
 *
 * Created by David Kepplinger on 2016-06-01.
 * Copyright (c) 2016 David Kepplinger. All rights reserved.
 *
 * This file is part of the R package MGL2.
 *
 * MGL2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MGL2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with R.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef rrlda2_BLAS_h
#define rrlda2_BLAS_h

#include <R_ext/BLAS.h>

#define BLAS_DSYMV(uplo, n, alpha, a, lda, x, incx, beta, y, incy)								\
	F77_CALL(dsymv)(uplo, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy);

#define BLAS_DGEMM(transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc)				\
	F77_CALL(dgemm)(transa, transb, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);

#define BLAS_DDOT(n, x, incx, y, incy)															\
	F77_CALL(ddot)(&n, x, &incx, y, &incy);

#define BLAS_DSCAL(n, alpha, x, incx)															\
	F77_CALL(dscal)(&n, &alpha, x, &incx);


#endif
