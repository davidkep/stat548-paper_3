#include <R.h>
#include <Rinternals.h>
#include <Rmath.h>
#include <stdlib.h>
#include <string.h>
#include <float.h> /* for DBL_MAX */

#include "BLAS.h"
#include "glasso.h"

#define RESULT_UNKNOWN -1
#define RESULT_OK 0
#define RESULT_CONV_THETA 1
#define RESULT_CONV_LATENT 2
#define RESULT_VAR_ZERO 3
#define RESULT_GLASSO_ADD 10

#define COV_OK 0
#define COV_VAR_ZERO 1


#define GLASSO_RESULT_OK 0
#define GLASSO_RESULT_CONV_EXT 2
#define GLASSO_RESULT_CONV_INT 3
#define GLASSO_RESULT_CONV_ACT 4

static const int BLAS_1I = 1;
static const double VAR_MIN = 1e-31;

#ifdef DEBUG
static void print_mati(int dr, int dc, const int *A, const char *header) {
    int r, c, i = 0;
    Rprintf("\n%s:\n", header);
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%2d ", A[i]);
        }
        Rprintf("\n");
    }
}

static void print_matf(int dr, int dc, const double *A, const char *header) {
    int r, c, i = 0;
    Rprintf("\n%s:\n", header);
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("% 09.5f  ", A[i]);
        }
        Rprintf("\n");
    }
}
#endif



static int calculateCovariance(const double *restrict const L, const int ndata, const int pLatent,
                               double *restrict const S, double *restrict const meanvec) {
    int i, j, k;
    int result = COV_OK;
    const double *restrict Lcol1;
    const double *restrict Lcol2;
    double tmp;

    Lcol1 = L;
	for (i = 0; i < pLatent; ++i) {
    	meanvec[i] = 0;
		for(k = 0; k < ndata; ++k) {
			meanvec[i] += *Lcol1;
		    ++Lcol1;
		}
		meanvec[i] /= ndata;
	}

	for (i = 0; i < pLatent; ++i) {
	    Lcol2 = &L[i * ndata]; /* Always points to the beginning of the j-th column */
		for (j = i; j < pLatent; ++j) {
		    Lcol1 = &L[i * ndata]; /* Always points to the beginning of the i-th column */
            tmp = 0;

            for (k = 0; k < ndata; ++k) {
                tmp += (*Lcol1 - meanvec[i]) * (*Lcol2 - meanvec[j]);
                ++Lcol1;
                ++Lcol2;
            }

            if (i == j & tmp < VAR_MIN) {
                result = COV_VAR_ZERO;
            }

		    S[i * pLatent + j] = S[j * pLatent + i] = tmp / (ndata - 1);
		}
	}

	return result;
}

static int performMGL(const double *restrict const data,
                      double *restrict const L, /* INOUT */
                      const int ndata,
                      const int pdata,
					  const int pLatent,
					  const double lambda,
					  const int maxiter,
					  double *restrict const threshold,
					  const int maxiter_glasso,
					  double *restrict const reltol_glasso,
					  const int penDiag_glasso,
					  const int printoutput,
					  double *restrict const Targ, /* OUT */
					  int *restrict const Z /* OUT */)
{
    const double factor = ndata / (ndata - 1);
	const size_t pLatents2 = (size_t) (pLatent * pLatent);
	const double reltolGlassoStart = *reltol_glasso;

	int result = RESULT_OK;
	int glasso_result;

	double tmp;
	double sumdiff;

	int i, j, k, iterLn;
	int covres;
	int itCountL = 0;
	int itCountWI = 0;

	double *tmpptr;

	double *restrict const S = (double *) R_alloc(pLatents2, sizeof(double));
	double *restrict Told = (double *) R_alloc(pLatents2, sizeof(double));
	double *restrict const W = (double *) R_alloc(pLatents2, sizeof(double));
	double *restrict const meanvec = (double *) R_alloc((size_t) pLatent, sizeof(double));
	unsigned int *restrict const moduleSize = (unsigned int*) R_alloc((size_t) pLatent,
                                            sizeof(unsigned int));

	double *restrict T = Targ;

	double *restrict iterL, *restrict iterT;
	const double *restrict iterData;

	double min_distance;
	double distance;

	double denom;
	double sumx, suml;

	/* update theta for initialization */
	covres = calculateCovariance(L, ndata, pLatent, S, meanvec);
	glasso_result = glasso(S, W, T, pLatent, lambda, maxiter_glasso, reltol_glasso, penDiag_glasso);
#ifdef DEBUG
	print_matf(pLatent, pLatent, S, "S");
	print_matf(pLatent, pLatent, T, "T");
#endif
    if (covres != COV_OK) {
        return RESULT_VAR_ZERO;
    }

	/* make Told and T the same */
	memcpy(Told, T, pLatents2 * sizeof(double));

	if (glasso_result != GLASSO_RESULT_OK) {
		return glasso_result + RESULT_GLASSO_ADD;
	}

	do {
		/************** UPDATE Z **************/
		memset(moduleSize, 0, (size_t) pLatent * sizeof(unsigned int));

		for (i = 0; i < pdata; ++i) {
			min_distance = DBL_MAX;
		    iterL = L;

			for (j = 0; j < pLatent; ++j) {
				distance = 0;
			    iterData = &data[i * ndata];

				for (k = 0; k < ndata; ++k){
                    tmp = *iterData - *iterL;
					distance += tmp * tmp;
					++iterL;
					++iterData;
				}

				if (distance < min_distance) {
					Z[i] = j;
					min_distance = distance;
				}
			}
			++moduleSize[Z[i]];
		}

		if(printoutput != 0) {
			Rprintf("MGL iteration %d: Z updated\n", itCountWI);
		}
#ifdef DEBUG
		print_mati(1, pdata, Z, "Z");
#endif

		/************** UPDATE L **************/
		itCountL = 0;
		do {
		    /* This copy is necessary, but a major slowdown */
		    sumdiff = 0;

		    iterLn = 0;
			/* start updating Ls */
			for (j = 0, iterT = T; j < pLatent; ++j, iterT += pLatent) {
				denom = 1 / (moduleSize[j] + factor * iterT[j]);
                /** WRONG???
                **/
				for (k = 0; k < ndata; ++k, ++iterLn) {
				    iterData = &data[k];
				    tmp = L[iterLn];
					// get x[k, s]
					sumx = 0;
					for(i = 0; i < pdata; ++i) {
						if(Z[i] == j) {
							sumx += *iterData;
						}
						iterData += ndata;
					}
					// get L[k, aa]%*%t

					suml = BLAS_DDOT(pLatent, iterT, BLAS_1I, L + k, ndata);
					suml -= L[iterLn] * iterT[j];

					L[iterLn] = (sumx - factor * suml) * denom;
					sumdiff += fabs(L[iterLn] - tmp);
				}
			}
		} while (sumdiff > *threshold && ++itCountL < maxiter);

		if (iterLn == maxiter) { /* not converged */
			Rprintf("MGL iteration %d: L update did not converge\n", itCountWI);
			*threshold = sumdiff;
			return RESULT_CONV_LATENT;
		} else if (printoutput != 0) {
			Rprintf("MGL iteration %d: L updated\n", itCountWI);
		}

#ifdef DEBUG
		print_matf(ndata, pLatent, L, "L");
#endif

		/************** UPDATE THETA **************/
		/*
		 * Let Told point to the old memory and T to the new one.
		 * We don't want to copy the matrix every time.
		 */
        tmpptr = T;
        T = Told;
        Told = tmpptr;

		covres = calculateCovariance(L, ndata, pLatent, S, meanvec);
		*reltol_glasso = reltolGlassoStart;
		glasso(S, W, T, pLatent, lambda, maxiter_glasso, reltol_glasso, penDiag_glasso);

#ifdef DEBUG
		print_matf(pLatent, pLatent, S, "S");
		print_matf(pLatent, pLatent, T, "T");
#endif
        if (covres != COV_OK) {
        	if (printoutput != 0) {
				Rprintf("MGL iteration %d: L update failed. At least one variable has variance too close to 0.\n", itCountWI);
			}
//             return RESULT_VAR_ZERO;
        }

		if (glasso_result != GLASSO_RESULT_OK) {
			if (printoutput != 0) {
				Rprintf("MGL iteration %d: Theta update failed. Glasso did not converge\n", itCountWI);
			}

			return glasso_result + RESULT_GLASSO_ADD;
		} else if (printoutput != 0) {
			Rprintf("MGL iteration %d: Theta updated\n", itCountWI);
		}

		sumdiff = 0;
		for (i = 0, k = 0; i < pLatent; ++i) {
            k += i + 1;
			for (j = i + 1; j < pLatent; ++j, ++k) {
				sumdiff += fabs(T[k] - Told[k]);
			}
		}

		if (printoutput != 0) {
			Rprintf("MGL iteration %d completed: sumdiff is %lf.\n", itCountWI, sumdiff);
		}

	} while (sumdiff > *threshold && ++itCountWI < maxiter);

	if (sumdiff > *threshold) { /* not converged */
		result = RESULT_CONV_THETA;
	}

	/*
	 * Check if the most up-to-date values of T reside in the memory of Told.
	 * If so, we need to copy them to the memory of Targ
	 */
	if ((itCountWI % 2) == 0) {
	    memcpy(Targ, T, pLatents2 * sizeof(double));
	}

	*threshold = sumdiff;
	return result;
}


SEXP C_MGL(SEXP Rdata,
           SEXP RL,
           SEXP Rndata,
           SEXP Rpdata,
           SEXP RpLatent,
           SEXP Rlambda,
           SEXP Rmaxiter,
		   SEXP Rthreshold,
		   SEXP Rmaxiter_glasso,
		   SEXP Rreltol_glasso,
		   SEXP RpenDiag_glasso,
		   SEXP Rprintoutput,
		   SEXP RT,
		   SEXP RZ) {

	int result = RESULT_UNKNOWN;

	result = performMGL(
		REAL(Rdata),
		REAL(RL),
		*INTEGER(Rndata),
		*INTEGER(Rpdata),
		*INTEGER(RpLatent),
		*REAL(Rlambda),
		*INTEGER(Rmaxiter),
		REAL(Rthreshold),
		*INTEGER(Rmaxiter_glasso),
		REAL(Rreltol_glasso),
		*INTEGER(RpenDiag_glasso),
		*INTEGER(Rprintoutput),
		REAL(RT),
		INTEGER(RZ)
	);

	return ScalarInteger(result);
}

