/*
 * config.h
 * rrlda2
 *
 * Created by David Kepplinger on 12.02.2015.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of the R package rrlda2.
 *
 * rrlda2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with R.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef rrlda2_config_h
#define rrlda2_config_h

#include "autoconfig.h"

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

#ifndef HAVE_UINT8_16_MAX
	typedef uint8_t unsigned char;
	typedef uint16_t unsigned short;
#endif

#endif
