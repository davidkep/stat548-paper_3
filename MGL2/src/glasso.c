/*
 * glasso.c
 * MGL2
 *
 * Created by David Kepplinger on 2016-06-01.
 * Copyright (c) 2016 David Kepplinger. All rights reserved.
 *
 * This file is part of the R package MGL2.
 *
 * MGL2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MGL2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with R.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 * The majority of the code in this file was taken from the R package huge
 * (v. 1.2.6) available at http://cran.r-project.org/web/packages/huge/
 * written by Tuo Zhao, Han Liu, Kathryn Roeder, John Lafferty, Larry Wasserman
 * and maintained by Tuo Zhao <tzhao5 at jhu.edu>
 */

#include "config.h"
#include "glasso.h"

#include <stdlib.h>
#include <stddef.h>
#include <Rmath.h>
#include <Rinternals.h>
#include <R_ext/Utils.h> /* for R_CheckUserInterrupt */
#include <string.h>

#include "BLAS.h"

#define MAX_ITER_INT_MULT 10

static const double BLAS_1F = 1.0;
static const int BLAS_1I = 1;

static const char * const BLAS_TRANS = "T";
static const char * const BLAS_NOTRANS = "N";
static const char * const BLAS_LEFT = "L";
static const char * const BLAS_LOWER = "L";


#ifdef DEBUG
static void print_mati(int dr, int dc, const int *A, const char *header) {
    int r, c, i = 0;
    Rprintf("\n%s:\n", header);
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%2d ", A[i]);
        }
        Rprintf("\n");
    }
}

static void print_matui16(int dr, int dc, const uint16_t *A, const char *header) {
    int r, c, i = 0;
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%2d ", A[i]);
        }
        Rprintf("\n");
    }
}

static void print_mati8(int dr, int dc, const int8_t *A, const char *header) {
    int r, c, i = 0;
    Rprintf("\n%s:\n", header);
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%2d ", A[i]);
        }
        Rprintf("\n");
    }
}

static void print_matf(int dr, int dc, const double *A, const char *header) {
    int r, c, i = 0;
    Rprintf("\n%s:\n", header);
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%8.6f  ", A[i]);
        }
        Rprintf("\n");
    }
}
#endif


/**
 * Calculate the glasso solution
 *
 * @param IN    S       The covariance matrix
 * @param INOUT W       The estimated regularized covariance matrix
 * @param INOUT T       The estimated regularized and sparse inverse of the covariance matrix
 *                      (precision matrix)
 * @param IN    d       The number of columns (and hence rows) of matrices S, W, and T (<= 2^16)
 * @param IN    lambda  The regularization parameter
 * @param IN    maxit   The maximum number of iterations of the outer loop. The inner loop is allowed
 *                      10 times more iterations
 * @param INOUT reltol  The relative tolerance of for the outer and the inner loops. When the algorithm
 *                      exits, it is set to the last value of the outer gap.
 *
 * @return An integer describing the result of the algorithm. 0 is OK, everything greater is an error
 */
int glasso(const double *restrict const S,
           double *restrict const W,
           double *restrict const T,
           const int d,
           const double lambda,
           const int maxit,
           double *restrict const reltol,
           const int penalizeDiagonal)
{
    int ret = GLASSO_RESULT_OK;
    const size_t ds = (size_t) d;
    const size_t d2 = ds * ds;
    const int maxitInt = maxit * MAX_ITER_INT_MULT;
    uint16_t col; /* Indices */
    uint32_t j, k, m; /* Indices */
    uint32_t colf, jf; /* The "flat" indices used */

    uint16_t rss_idx, w_idx;

    uint16_t gap_int;
    double betaRelChange;
    uint16_t iter_ext, iter_int, iter_act;

    /*
     * active sets (shifted by +1)
     * Stores for each column the indices of the active columns (in the first activeSize places)
     * can be left unset as only previously set indices are accessed
     *
     */
    uint16_t *restrict activeIndices = (uint16_t*) malloc(d2 * sizeof(uint16_t));

    /* inactive sets: 0 means inactive, 1 means active */
    int8_t *restrict inactives = (int8_t*) malloc(d2 * sizeof(int8_t));

    /* sizes of active sets for each column */
    uint16_t *restrict activeSize = (uint16_t*) malloc(ds * sizeof(uint16_t));
    double *restrict tmpVec = (double*) malloc(ds * sizeof(double));
    double *restrict betaPrev = (double*) malloc(ds * sizeof(double)); /* store previous beta values to check for convergence */

    uint16_t activeSizePrev; /* original size of the active set */
    uint16_t junkCount; /* the number of variables returning to the inactive set from the active set */

    double r; //partial residual
    double betaChange, betaAbsSum, betaTotalAbsSum, betaTotalChange, betaTotalRelChange;
    double tmp;

    /*
     * Until the end ("Compute the final T"),
     * T is used to hold the beta's for each individual lasso, i.e., the j-th column of T is
     * the beta of regressing the other data on the j-th column
     */


	/*
	 * Construct initial solutions
	 */
	memcpy(W, S, d2 * sizeof(double));
	memset(T, 0.0, d2 * sizeof(double));
	memset(activeSize, 0, ds * sizeof(uint16_t));
	memset(inactives, 1, d2 * sizeof(int8_t));
	for (col = 0, colf = 0; col < d; ++col, colf += d) {
	    /* The diagonal elements of W are set optimal */
		W[colf + col] += penalizeDiagonal * lambda;
		inactives[colf + col] = 0;
	}

    iter_ext = 0;
    betaTotalRelChange = 1 + (*reltol);

    R_CheckUserInterrupt();

    /*
     * Outer loop
     */
    while (betaTotalRelChange > *reltol && iter_ext++ < maxit && ret == GLASSO_RESULT_OK) {
        betaAbsSum = 0;
        betaTotalAbsSum = 0;
        betaTotalChange = 0;
        for (col = 0, colf = 0; col < d; ++col, colf += d) {
            gap_int = 1;
            iter_int = 0;

            memcpy(betaPrev, T + colf, ds * sizeof(double));

            /*
             * Inner loop
             * Find LASSO estimate for current column
             */
            while (gap_int != 0 && iter_int++ < maxitInt && ret == GLASSO_RESULT_OK) {

                activeSizePrev = activeSize[col];
                for (j = 0, jf = 0; j < d; ++j, jf += d) {
                    if (inactives[colf + j] > 0) {
                        r = S[colf + j];
                        for (k = colf, m = colf + activeSize[col]; k < m; ++k) {
                            rss_idx = activeIndices[k] - 1;
                            r -= W[jf + rss_idx] * T[colf + rss_idx];
                        }

                        if (r > lambda) {
                            tmpVec[j] = (r - lambda) / W[jf + j];
                            activeIndices[colf + activeSize[col]] = j + 1;
                            ++activeSize[col];
                            inactives[colf + j] = 0;
                        } else if (r < -lambda) {
                            tmpVec[j] = (r + lambda) / W[jf + j];
                            activeIndices[colf + activeSize[col]] = j + 1;
                            ++activeSize[col];
                            inactives[colf + j] = 0;
                        } else {
                            tmpVec[j] = 0;
                        }

                        T[colf + j] = tmpVec[j];
                    }
                }

                gap_int = activeSize[col] - activeSizePrev;

                betaRelChange = 1;
                iter_act = 0;

                while (betaRelChange > *reltol && iter_act++ < maxitInt && ret == GLASSO_RESULT_OK) {
                    betaChange = 0;
                    betaAbsSum = 0;
                    for (j = 0; j < activeSize[col]; ++j) {
                        w_idx = activeIndices[colf + j];
                        if (w_idx > 0) {
                            --w_idx;

                            jf = w_idx * d;
                            r = S[colf + w_idx] + T[colf + w_idx] * W[jf + w_idx];
                            for (k = colf, m = colf + activeSize[col]; k < m; ++k) {
                                rss_idx = activeIndices[k] - 1;
                                r -= W[jf + rss_idx] * T[colf + rss_idx];
                            }

                            if (r > lambda) {
                                tmpVec[w_idx] = (r - lambda) / W[jf + w_idx];
                                betaAbsSum += tmpVec[w_idx];
                            } else if (r < -lambda) {
                                tmpVec[w_idx] = (r + lambda) / W[jf + w_idx];
                                betaAbsSum -= tmpVec[w_idx];
                            } else {
                                tmpVec[w_idx] = 0;
                            }

                            betaChange += fabs(tmpVec[w_idx] - T[colf + w_idx]);

                            T[colf + w_idx] = tmpVec[w_idx];
                        }
                    }
                    betaRelChange = betaChange / betaAbsSum;
                }

                if (iter_act >= maxitInt) {
                    ret = GLASSO_RESULT_CONV_ACT;
                    break;
                }

                /* move the false active variables to the inactive set */
                junkCount = 0;
                for (j = 0; j < activeSize[col]; ++j) {
                    w_idx = activeIndices[colf + j] - 1;
                    if (tmpVec[w_idx] == 0) {
                        ++junkCount;
                        inactives[colf + w_idx] = 1;
                        activeIndices[colf + j] = 0;
                    } else {
                        activeIndices[colf + j - junkCount] = w_idx + 1;
                    }
                }
                activeSize[col] -= junkCount;
            }

            if (iter_int >= maxitInt) {
                ret = GLASSO_RESULT_CONV_INT;
            }

            R_CheckUserInterrupt();

            /*
             * Update w12 = W11 . B
             * and calculate the absolute change of beta
             */
            memcpy(tmpVec, W + colf, ds * sizeof(double));
            tmp = -T[colf + col];
            BLAS_DSYMV(BLAS_LOWER, d, BLAS_1F, W, d, T + colf, BLAS_1I, tmp, tmpVec, BLAS_1I);

            for (m = 0, jf = colf, k = col; m < col; ++m, ++jf, k += d) {
                W[jf] = W[k] = tmpVec[m];
                betaTotalChange += fabs(betaPrev[m] - T[jf]);
            }

            betaTotalChange += fabs(betaPrev[m] - T[jf]);

            for (++m, ++jf, k += d; m < d; ++m, ++jf, k += d) {
                W[jf] = W[k] = tmpVec[m];
                betaTotalChange += fabs(betaPrev[m] - T[jf]);
            }

            betaTotalAbsSum += betaAbsSum;
        }

        betaTotalRelChange = betaTotalChange / betaTotalAbsSum;

        R_CheckUserInterrupt();
    }

    *reltol = betaTotalRelChange;

    if (iter_ext >= maxitInt) {
        ret = GLASSO_RESULT_CONV_EXT;
    }

    /*
     * Compute the final T and the degrees of freedom
     */
    for (col = 0, colf = 0; col < d; ++col, colf += d) {
        tmp = BLAS_DDOT(d, W + colf, BLAS_1I, T + colf, BLAS_1I);
        tmp = 1 / (tmp - W[colf + col] * T[colf + col] - W[colf + col]);
        BLAS_DSCAL(d, tmp, T + colf, BLAS_1I);

        T[colf + col] = -tmp;
    }
	/*
	 * Free allocated memory
	 */
    free(activeIndices);
    free(inactives);
	free(activeSize);
    free(tmpVec);
    free(betaPrev);

    return ret;
}


SEXP C_glasso(SEXP RS,
              SEXP RW,
              SEXP RT,
              SEXP Rd,
              SEXP Rlambda,
              SEXP Rmaxit,
              SEXP Rreltol,
              SEXP RpenalizeDiagonal)
{
    int result = GLASSO_RESULT_UNKNOWN;
    result = glasso(REAL(RS),
                    REAL(RW),
                    REAL(RT),
                    *INTEGER(Rd),
                    *REAL(Rlambda),
                    *INTEGER(Rmaxit),
                    REAL(Rreltol),
                    *INTEGER(RpenalizeDiagonal));
    return ScalarInteger(result);
}
