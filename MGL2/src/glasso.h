/*
 * glasso.h
 * MGL2
 *
 * Created by David Kepplinger on 2016-06-01.
 * Copyright (c) 2016 David Kepplinger. All rights reserved.
 *
 * This file is part of the R package MGL2.
 *
 * MGL2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MGL2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with R.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef glasso_h
#define glasso_h

#include <Rinternals.h>

#define GLASSO_RESULT_UNKNOWN -1
#define GLASSO_RESULT_OK 0
#define GLASSO_RESULT_CONV_EXT 2
#define GLASSO_RESULT_CONV_INT 3
#define GLASSO_RESULT_CONV_ACT 4

/**
 * Calculate the glasso solution
 *
 * @param IN    S       The covariance matrix
 * @param INOUT W       The estimated regularized covariance matrix
 * @param INOUT T       The estimated regularized and sparse inverse of the covariance matrix
 *                      (precision matrix)
 * @param IN    d       The number of columns (and hence rows) of matrices S, W, and T (<= 2^16)
 * @param IN    lambda  The regularization parameter
 * @param IN    maxit   The maximum number of iterations of the outer loop. The inner loop is allowed
 *                      10 times more iterations
 * @param INOUT reltol  The relative tolerance of for the outer and the inner loops. When the algorithm
 *                      exits, it is set to the last value of the outer gap.
 *
 * @return An integer describing the result of the algorithm. 0 is OK, everything greater is an error
 */
int glasso(const double *restrict const S,
           double *restrict const W,
           double *restrict const T,
           const int d,
           const double lambda,
           const int maxit,
           double *restrict const reltol,
           const int penalizeDiagonal);

SEXP C_glasso(SEXP RS,
              SEXP RW,
              SEXP RT,
              SEXP Rd,
              SEXP Rlambda,
              SEXP Rmaxit,
              SEXP Rreltol,
              SEXP RpenalizeDiagonal);

#endif /* glasso_h */
