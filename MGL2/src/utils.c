/*
 * utils.c
 * MGL2
 *
 * Created by David Kepplinger on 12.02.2015.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of the R package rrlda2.
 *
 * rrlda2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with R.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 * The majority of the code in this file was taken from the R package huge
 * (v. 1.2.6) available at http://cran.r-project.org/web/packages/huge/
 * written by Tuo Zhao, Han Liu, Kathryn Roeder, John Lafferty, Larry Wasserman
 * and maintained by Tuo Zhao <tzhao5 at jhu.edu>
 */

#include "config.h"

#include <stddef.h>
#include <R.h>
#include <Rmath.h>
#include <Rinternals.h>

/**
 * Screen the off-diagonal of the input matrix for entries absolutely larger than lambda and
 * return the vector of 1-based indices of the columns with those entries.
 *
 * @param IN double RX The matrix X
 * @param IN double Rlambda The cutoff value lambda
 * @param IN int Rncol The number of colums in X
 * @param IN int Rnrow The total number of rows in X
 */
SEXP C_screen(SEXP RX, SEXP Rlambda, SEXP Rncol, SEXP Rnrow) {
	const int nrow = INTEGER(Rnrow)[0],
			  ncol = INTEGER(Rncol)[0];
	const double lambda = REAL(Rlambda)[0];
	const double *X = REAL(RX);
	int j, found;
	SEXP Rindices;
	int *restrict tmpRindices;
	int *restrict indices = (int*) R_alloc(ncol, sizeof(int));
	int nres = 0;

	for (int i = 0; i < ncol; ++i, X += nrow) {
		found = 0;

		for(j = 0; j < i; ++j) {
			if (fabs(*(X + j)) > lambda) {
				found = 1;
				break;
			}
		}
		for(++j; j < nrow && found == 0; ++j) {
			if (fabs(*(X + j)) > lambda) {
				found = 1;
				break;
			}
		}

		if (found == 1) {
			indices[nres] = i + 1;
			++nres;
		}
	}

	Rindices = PROTECT(allocVector(INTSXP, nres));
	tmpRindices = INTEGER(Rindices);

	for (j = 0; j < nres; ++j) {
		tmpRindices[j] = indices[j];
	}

	UNPROTECT(1);

	return Rindices;
}
