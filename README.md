# STAT 548 – Paper 3 #
## Efficient Dimensionality Reduction for High-Dimensional Network Estimation ##

This repository hosts code to run simulations and experiments similar to the ones conducted
in
S. Celik, B. Logsdon, and S.-I. Lee. “Effcient dimensionality reduction for high-dimensional
network estimation”. In: Proceedings of the 31st international conference on machine learning
(icml-14). 2014, pp. 1953–1961.
The R implementation of the MGL algorithm can be found in the folder
[MGL2](MGL2). This is an updated version of the original
[MGL](https://cran.r-project.org/package=MGL) R package developed for the paper by its authors.


## License ##
The code is provided under the GNU GPL v3, for more informations see [LICENSE.txt](./LICENSE.txt).
The R package __MGL2__ is licensed under GNU GPL v2 and license information is included
in the package.
